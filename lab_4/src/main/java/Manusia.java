public class Manusia {
    private String nama = "";
    private int umur = 0;
    private float kebahagiaan = 50;
    private int totalUang;
    private static Manusia last;
    private boolean hidup = true;

    public Manusia(String nama, int umur, int totalUang){
        this.nama = nama;
        this.umur = umur;
        this.totalUang = totalUang;
        last = this;
    }
    public Manusia(String nama, int umur){
        this.nama = nama;
        this.umur = umur;
        this.totalUang = 50000;
        last = this;
    }

    public void setName(String nama){
        this.nama = nama;
    }
    public void setUmur(int umur){
        this.umur = umur;
    }
    public void setUang(int totalUang){
        this.totalUang = totalUang;
    }
    public void setKebahagiaan(float kebahagiaan){
        this.kebahagiaan = kebahagiaan;
    }
    public void setHidup(boolean x){
        this.hidup = x;
    }

    public String getNama(){
        return this.nama;
    }
    public int getUmur(){
        return this.umur;
    }
    public int getUang(){
        return this.totalUang;
    }
    public float getKebahagiaan(){
        return this.kebahagiaan;
    }
    public Manusia getLast(){
        return last;
    }

    public void beriUang(Manusia penerima) {
        if (this.hidup == false) {
            System.out.println(this.nama + " sudah tiada :(");
        } else if (penerima.hidup == false) {
            System.out.println(penerima.nama + " sudah tiada :(");
        } else {
            int totalAscii = 0;
            for (int i = 0; i < penerima.getNama().length(); i++) {
                char huruf = penerima.getNama().charAt(i);
                int ascii = (int) huruf;
                totalAscii += ascii;
            }
            int uang = totalAscii * 100;
            if ((this.totalUang - uang) > 0) {
                this.totalUang -= uang;
                penerima.setUang(penerima.getUang() + uang);
                this.kebahagiaan += (float) (uang) / 6000;
                penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)(uang) / 6000);
                if (this.kebahagiaan > 100) {
                    this.kebahagiaan = 100;
                }
                if (penerima.getKebahagiaan() > 100) {
                    penerima.setKebahagiaan(100);
                }
                System.out.println(this.nama + " memberi uang sebanyak " + uang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            } else {
                System.out.println(this.nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            }
        }
    }

    public void beriUang(Manusia penerima, int uang){
        if(this.hidup==false){
            System.out.println(this.nama + " sudah tiada :(");
        }else if(penerima.hidup==false){
            System.out.println(penerima.nama + " sudah tiada :(");
        }else{
            if((this.totalUang - uang)>0){
                this.totalUang -= uang;
                penerima.setUang(penerima.getUang()+uang);
                this.kebahagiaan += (float)(uang)/6000;
                penerima.setKebahagiaan(penerima.getKebahagiaan() + (float) (uang)/6000);
                if(this.kebahagiaan>100){
                    this.kebahagiaan = 100;
                }if(penerima.getKebahagiaan()>100){
                    penerima.setKebahagiaan(100);
                }
                System.out.println(this.nama + " memberi uang sebanyak " + uang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }else{
                System.out.println(this.nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
            }
        }
    }

    public void bekerja(int durasi, int bebanKerja){
        if(this.hidup == false){
            System.out.println(this.nama + " sudah tiada :(");
        }else{
            int bebanKerjaTotal = 0;
            int pendapatan = 0;
            if(this.umur<18){
                System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
            }else{
                bebanKerjaTotal = durasi*bebanKerja;
                if(this.kebahagiaan>bebanKerjaTotal){
                    this.kebahagiaan -= bebanKerjaTotal;
                    pendapatan = bebanKerjaTotal*10000;
                    System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
                }else{
                    durasi = (int)this.kebahagiaan/bebanKerja;
                    bebanKerjaTotal = durasi * bebanKerja;
                    pendapatan = bebanKerjaTotal * 10000;
                    this.kebahagiaan -= bebanKerjaTotal;
                    System.out.println(this.nama + " tidak bekerja karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                }
                this.totalUang += pendapatan;
            }
        }
    }

    public void rekreasi(String namaTempat){
        if(this.hidup==false){
            System.out.println(this.nama + " sudah tiada :(");
        }else{
            int biaya = namaTempat.length() * 10000;
            if (this.totalUang < biaya){
                System.out.println(this.nama + " tidak memiliki cukup uang untuk berekreasi di " + namaTempat + " :(");
            }
            else{
                this.totalUang -= biaya;
                this.kebahagiaan += namaTempat.length();
                if(this.getKebahagiaan() > 100){
                    this.setKebahagiaan(100);
                }
                System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
            }
        }
    }

    public void sakit(String namaPenyakit){
        if(this.hidup == false){
            System.out.println(this.nama + " sudah tiada :(");
        }
        else{
            this.kebahagiaan -= namaPenyakit.length();
            if(this.getKebahagiaan() < 0){
                this.setKebahagiaan(0);
            }
            System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
        }
    }

    public String toString(){
        return "Nama\t\t: " + this.nama + "\nUmur\t\t: " + this.umur + "\nUang\t\t: " + this.totalUang + "\nKebahagiaan\t: " + this.kebahagiaan;
    }

    public void meninggal(){
        if(this.hidup == false){
            System.out.println(this.nama + " sudah tiada :(");
        }
        else{
            this.setHidup(false);
            System.out.println(this.nama + " telah meninggal dengan tenang, kebahagiaan : " + this.kebahagiaan);
            this.setName("Almarhum " + this.nama);
            if(this == last){
                System.out.println("Harta milik " + this.nama + " telah hangus");
                this.totalUang = 0;
            }
            else{
                last.totalUang += this.totalUang;
                this.totalUang = 0;
                System.out.println("Semua harta milik " + this.nama + " telah diwariskan kepada " + last.nama);
            }
        }
    }
}