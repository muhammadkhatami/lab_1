import java.util.Scanner;
//Muhammad Khatami
public class RabbitHouse {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String inputUser = input.nextLine().toLowerCase();
        String [] hasilSplit = inputUser.split(" ");
        String jenis = hasilSplit[0];
        String nama = hasilSplit[1];

        if(jenis.equals("normal")){
            int panjang = nama.length();
            int hasil = jumlah(panjang);
            System.out.println(hasil);
        }else if(jenis.equals("palindrom")){
            int result = palindrom(nama, 0);
            System.out.println(result);
        }
    }

    public static int jumlah(int panjang) {
        if (panjang == 1) {
            return 1;
        } else {
            return jumlah(panjang - 1)*panjang + 1;
        }
    }

    public static int palindrom (String nama, int posisi){
        String backward = new StringBuffer(nama).reverse().toString();
        if ((nama.length()==posisi) && !(backward.equals(nama))){
            return 1;
        }if(backward.equals(nama)){
            return 0;
        }if(posisi>nama.length()){
            return 0;
        }
        String anak = nama.substring(0,posisi) + nama.substring(posisi+1);
        return palindrom(nama, posisi+1) + palindrom(anak, 0);
    }
}