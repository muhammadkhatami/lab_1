import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

class Lab8 {
    private static int batasGaji;

    public static void main(String[] args) throws FileNotFoundException, IOException {

        File file = new File("D:\\UI\\Semester 2\\DDP-2"
                + "\\Kuliah\\TutorialDDP2\\lab_8\\src\\main\\java\\input.txt");

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String readInputFile;
        while ((readInputFile = bufferedReader.readLine()) != null) {
            String[] command = readInputFile.split(" ");
            if (command[0].equalsIgnoreCase("tambah_karyawan")) {
                Office.add(command[1], command[2], Integer.parseInt(command[3]));
            } else if (command[0].equalsIgnoreCase("status")) {
                Office.status(command[1]);
            } else if (command[0].equalsIgnoreCase("tambah_bawahan")) {
                Office.tambahBawahan(command[1],command[2]);
            } else if (command[0].equalsIgnoreCase("gajian")) {
                Office.gajian();
            } else if (command.length == 1) {
                try {
                    int batasGaji = Integer.parseInt(command[0]);
                    setBatasGaji(batasGaji);
                } catch (Exception batasGaji) {
                    System.out.println("Perintah yang anda masukan salah");
                } finally {
                    continue;
                }
            } else {
                System.out.println("Perintah yang anda masukan salah");
            }
        }
    }

    public static int setBatasGaji(int batasGaji) {
        return batasGaji = batasGaji;
    }

    public static int getBatasGaji() {
        return batasGaji;
    }
}