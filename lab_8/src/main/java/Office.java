import karyawan.Intern;
import karyawan.Karyawan;
import karyawan.Manager;
import karyawan.Staff;

public class Office {
    private static Karyawan[] karyawanList = new Karyawan[10000];
    private static int countKaryawan = 0;
    private static int batasGaji = Lab8.getBatasGaji();

    public static void add(String name, String jabatan, int gaji) {
        if (findName(name) != null) {
            System.out.println("Karyawan dengan nama " + name.toUpperCase() + " telah terdaftar");
        } else if (jabatan.equalsIgnoreCase("manager")) {
            karyawanList[countKaryawan] = new Manager(name, gaji);
        } else if (jabatan.equalsIgnoreCase("staff")) {
            karyawanList[countKaryawan] = new Staff(name, gaji);
        } else if (jabatan.equalsIgnoreCase("intern")) {
            karyawanList[countKaryawan] = new Intern(name, gaji);
        }
        countKaryawan += 1;
        System.out.println(name + " telah bekerja sebagai " + jabatan    + " di PT. NAMPAN");
    }

    public static Karyawan findName(String name) {
        String tidakAda = "Nama tidak ditemukan";
        for (int i = 0; i < 10000; i++) {
            if (karyawanList[i] == null) {
                continue;
            } else if (karyawanList[i].getName().equalsIgnoreCase(name)) {
                return karyawanList[i];
            }
        }
        return null;
    }

    public static void tambahBawahan(String atasan, String bawahan) {
        Karyawan boss = findName(atasan);
        Karyawan anakBuah = findName(bawahan);
        if (atasan != null || bawahan != null) {
            System.out.println("Nama tidak berhasil ditemukkan");
            return;
        } else {
            System.out.println(boss.tambahAnakBuah(anakBuah));
        }
    }

    public static void status(String name) {
        Karyawan namaKaryawan = findName(name);
        if (findName(name) == null) {
            System.out.println("Karyawan tidak ditemukan");
            return;
        }
        System.out.println(namaKaryawan.getName() + " " + namaKaryawan.getGaji());
    }

    public static void gajian() {
        System.out.println("Semua karyawan telah diberikan gaji");
        for (int i = 0; i < 10000; i++) {
            if (karyawanList[i] != null) {
                karyawanList[i].gajian();
                if (karyawanList[i] instanceof Staff
                        && karyawanList[i].getGaji() > batasGaji) {
                    karyawanList[i] = new Manager(karyawanList[i].getName(),
                            karyawanList[i].getGaji());
                    System.out.println("Selamat, " + karyawanList[i].getName()
                            + " telah dipromosikan menjadi MANAGER");
                }
            } else {
                continue;
            }
        }
    }
}