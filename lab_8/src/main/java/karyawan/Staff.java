package karyawan;

public class Staff extends Karyawan {
    private Karyawan[] listBawahan = new Karyawan[10];
    private int countListBawahan = 0;

    public Staff(String name, int gaji) {
        super(name, gaji);
    }

    public String tambahAnakBuah(Karyawan bawahan) {
        for (int i = 0; i < 10; i++) {
            if (bawahan.getName().equalsIgnoreCase(listBawahan[i].getName())) {
                return "Karyawan " + bawahan.getName() + " telah menjadi bawahan " + this.name;
            }
        }
        if (bawahan instanceof Intern) {
            if (countListBawahan != 9) {
                listBawahan[countListBawahan] = bawahan;
                countListBawahan += 1;
                return "Karyawan " + bawahan.getName()
                        + " berhasil ditambahkan menjadi bawahan " + this.name;
            } else {
                return "Anda tidak bisa lagi menambahkan bawahan";
            }
        } else {
            return "hanya Intern yang bisa menjadi bawahan anda";
        }
    }
}
