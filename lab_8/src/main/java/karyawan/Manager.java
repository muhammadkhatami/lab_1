package karyawan;

public class Manager extends Karyawan {
    private Karyawan[] listBawahan = new Karyawan[10];
    private int countListBawahan = 0;

    public Manager(String name, int gaji) {
        super(name, gaji);
    }

    public String tambahAnakBuah(Karyawan bawahan) {
        for (int i = 0; i < 10; i++) {
            if (bawahan.getName().equalsIgnoreCase(listBawahan[i].getName())) {
                return "Karyawan " + bawahan.getName() + " telah menjadi bawahan " + this.name;
            }
        }
        if (!(bawahan instanceof Manager)) {
            if (countListBawahan != 9) {
                listBawahan[countListBawahan] = bawahan;
                countListBawahan += 1;
                return "Karyawan " + bawahan.getName()
                        + " berhasil ditambahkan menjadi bawahan " + this.name;
            } else {
                return "Anda tidak bisa lagi menambahkan bawahan";
            }
        } else {
            return "Manager tidak bisa anda jadikan bawahan anda";
        }
    }
}
