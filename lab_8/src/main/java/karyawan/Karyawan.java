package karyawan;

public class Karyawan {
    protected String name;
    protected int gaji;
    protected int banyakGajian = 0;

    public Karyawan(String name, int gaji) {
        this.name = name;
        this.gaji = gaji;
    }

    public String getName() {
        return name;
    }

    public int setGaji(int gajiBaru) {
        return this.gaji = gajiBaru;
    }

    public int getGaji() {
        return gaji;
    }

    public String tambahAnakBuah(Karyawan bawahan) {
        if (this instanceof Manager) {
            return ((Manager) this).tambahAnakBuah(bawahan);
        } else if (this instanceof Staff) {
            return ((Staff) this).tambahAnakBuah(bawahan);
        } else {
            return "Anda tidak dapat memiliki bawahan";
        }
    }

    public void gajian() {
        banyakGajian += 1;
        if (banyakGajian % 6 == 0) {
            naikGaji();
        }
    }

    public void naikGaji() {
        int gajiBaru = gaji + (gaji / 10);
        banyakGajian += 1;
        System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari "
                + gaji + " menjadi " + gajiBaru);
        gaji = gajiBaru;
    }
}
