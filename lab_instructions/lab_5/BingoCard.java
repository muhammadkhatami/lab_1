/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
<<<<<<< HEAD:lab_5/src/main/java/BingoCard.java
 * test test
=======
>>>>>>> f78c0e7742338c8c37b272c05a7ad43888206b02:lab_instructions/lab_5/BingoCard.java
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[][] cleanNumbers;
	private boolean isBingo;

	public BingoCard(Number [][] numbers, Number[][] cleanNumbers){
		this.numbers = numbers;
		this.cleanNumbers = cleanNumbers;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		String hasil = "";
		boolean ketemu = true;
		for(int i = 0; i<5; i++) {
			for(int j = 0; j<5; j++) {
				if(numbers[i][j].getValue() == num) {
					if(numbers[i][j].getValue() == 'X') {
						ketemu = false;
						hasil += String.valueOf(num) + "sebelumnya sudah tersilang";
						break;
					}
					else if(numbers[i][j].getValue() != 1) {
						ketemu = false;
						numbers[i][j].setValue('X');
						hasil += String.valueOf(num) + " tersilang";
						this.checkBingo();
						break;
					}
				}

			}

		}
		if(ketemu == true) {
				hasil += "Kartu tidak memiliki angka " + String.valueOf(num);
		}
		return hasil;
	}

	public String info() {
		String result = "";
		for(int i = 0; i<5; i++) {
			for(int j = 0; j<5; j++) {
				result += "| ";
				if(numbers[i][j].getValue() == 'X') {
					result += String.format("%c", numbers[i][j]);
					if(j==4) {
						if(i==4) {
							result += "| ";
						}
						else{
							result += "| ";
							result += "\n";
						}
					}
				}
				else {
					result += String.valueOf(numbers[i][j]);
					if(j==4) {
						if(i==4) {
							result += "| ";
						}
						else{
							result += "| ";
							result += "\n";
						}
					}
				}
			}
		}
		return result;
	}

	public void restart(){
		this.numbers = this.cleanNumbers;
		System.out.println("Mulligan!");
	}

	public void checkBingo() {
		for(int i = 0; i<5; i++) {
			int check = 0;
			for(int j = 0; j<5; j++) {
				if(numbers[i][j].getValue() == 'X'){
					check += 1;
				}
				else{
					break;
				}
				if(check == 5) {
					System.out.println("BINGO!");
					System.exit(0);
					break;
				}
			}
		}
		for(int j = 0; j<5; j++) {
			int check = 0;
			for(int i = 0; i<5; i++) {
				if(numbers[i][j].getValue() == 'X'){
					check += 1;
				}
				else{
					break;
				}
				if(check == 5) {
					System.out.println("BINGO!");
					System.exit(0);
					break;
				}
			}
		}
		int cek = 0;
		for(int i=0; i<5; i++) {
			for(int j=0; j<5; j++) {
				if(numbers[i][j].getValue() == 'X') {
					if(i == j) {
						cek += 1;
						if(cek == 5) {
							System.out.println("BINGO!");
							System.exit(0);
							break;
						}
					}
				}
			}
		}
		int ngecek = 0;
		for(int i=0; i<5; i++) {
			for(int j=0; j<5; j++){
				if(numbers[i][j].getValue() == 'X') {
					if(i+j == 4) {
						ngecek += 1;
						if(ngecek == 5) {
							System.out.println("BINGO!");
							System.exit(0);
							break;
						}
					}
				}
			}
		}
	}
}
