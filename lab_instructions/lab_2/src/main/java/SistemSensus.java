import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 * 
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Muhammad Khatami, NPM 1706044055, Kelas DDP2-F GitLab Account: muhammadkhatami
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang = Short.parseShort(input.nextLine());
		if(panjang>250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		short lebar = Short.parseShort(input.nextLine());
		if(lebar>250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi = Short.parseShort(input.nextLine());
		if(tinggi>250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		if(berat>150){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0); 	
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		short makanan = Short.parseShort(input.nextLine());
		if(makanan>20){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		short jumlahCetakan = Short.parseShort(input.nextLine());

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		
		short rasio = (short) (berat*1000000/(panjang*tinggi*lebar));
		short total = 0;
		for (int i=1; i<jumlahCetakan+1; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.println("");
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() == 0){
				catatan = "Tidak ada catatan tambahan";
			}else{
				catatan = catatan;
				}

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "";
			System.out.println("DATA SIAP DICETAK UNTUK "+ penerima);
			System.out.println("--------------------");
			System.out.println(nama + " - " + alamat);
			System.out.println("Lahir pada tanggal " + tanggalLahir);
			System.out.println("Rasio Berat Per Volume     = " + rasio + " kg/m^3");
			System.out.println("catatan: " + catatan);
		}
		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		char[] ch = nama.toCharArray();
		for (char c : ch){
			short tes = (short)c;
			total += tes;	
		}
		int kodeAngka = total%10000;
		
		String kodeHuruf = nama.substring(0,1);

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = kodeHuruf + kodeAngka;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000*365*makanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String [] tahun = tanggalLahir.split("-");
		short tahunLahir = Short.parseShort(tahun[2]); // lihat hint jika bingung
		int umur = 2018-tahunLahir;		

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaTempat;
		String kabupaten;
		
		if(umur <= 18){
			namaTempat = "PPMT";
			kabupaten = "Rotunda";
		}else if(umur>=19 && umur <= 1018 && anggaran<100000000){
			namaTempat = "Teksas";
			kabupaten = "Sastra";
		}else{
			namaTempat = "Mares";
			kabupaten = "Margonda";
		}		

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "";
		//.....;
		System.out.println("");
		System.out.println("PROGRAM PENCETAK DATA SENSUS"); 
		System.out.println("--------------------");
		System.out.println("MENGETAHUI: Identitas keluarga: "+nama+" - "+nomorKeluarga);
		System.out.println("MENIMBANG:  Anggaran makanan tahunan: Rp "+anggaran);
		System.out.println("            Umur kepala keluarga: "+umur+" tahun");
		System.out.println("MEMUTUSKAN: keluarga "+nama+" akan ditempatkan di:");
		System.out.println(namaTempat + ", " + kabupaten);
		
		input.close();
	}
}
