package customer;

import theater.Theater;
import ticket.Ticket;

public class Customer {
    private String name;
    private boolean gender;
    private int age;

    public Customer(String name, boolean gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public void findMovie(Theater theaterName, String title) {
        for (int i = 0; i<theaterName.getMovies().length; i++) {
            if(theaterName.getMovies()[i].getTitle().equals(title)){
                System.out.println(theaterName.getMovies()[i].printMovie());
                break;
            }
            else if(i == theaterName.getMovies().length-1){
                System.out.println("Film " + title + " yang dicari " + this.name + " tidak ada di bioskop "
                        + theaterName.getName());
            }
        }
    }

    public Ticket orderTicket(Theater theaterName, String title, String day, String type){
        for(int i = 0; i < theaterName.getTicket().size(); i++){
            if(theaterName.getTicket().get(i).getMovie().getTitle().toLowerCase().equals(title.toLowerCase()) &&
                    theaterName.getTicket().get(i).getDay().toLowerCase().equals(day.toLowerCase()) &&
                    theaterName.getTicket().get(i).getType().toLowerCase().equals(type.toLowerCase())){
                if(this.age >= theaterName.getTicket().get(i).getMovie().getRating()) {
                    int profit = theaterName.getTicket().get(i).getPrice();
                    System.out.println(this.name + " telah membeli tiket " + title + " jenis " + type + " di " +
                            theaterName.getName() + " pada hari " + theaterName.getTicket().get(i).getDay()
                            + " seharga Rp. " + profit);
                    theaterName.setTheaterAsset(profit);
                    return theaterName.getTicket().get(i);
                }
                else{
                    int minAge = theaterName.getTicket().get(i).getMovie().getRating();
                    String rate = "";
                    if(minAge == 13){
                        rate = "Remaja";
                    }
                    else{
                        rate = "Dewasa";
                    }
                    System.out.println(this.name + " masih belum cukup umur untuk menonton " + title +
                            " dengan rating " + rate);
                    return null;
                }
            }
        }
        System.out.println("Tiket untuk film " + title + " jenis " + type + " dengan jadwal " + day +
                " tidak tersedia di " + theaterName.getName());
        return null;
    }
}
