package ticket;

import movie.Movie;

public class Ticket{
    private Movie movie;
    private String day;
    private boolean threeD;
    private int price = 60000;

    public Ticket(Movie title, String day, boolean threeD) {
        this.movie = title;
        this.day = day;
        this.threeD = threeD;
    }

    public String getDay() {
        return this.day;
    }

    public String getType() {
        if(this.threeD == true) {
            return "3 Dimensi";
        }
        else {
            return "biasa";
        }
    }

    public int getPrice() {
        if(this.day.toLowerCase().equals("sabtu") || this.day.toLowerCase().equals("minggu")) {
            this.price = 100000;
        }
        if(this.threeD == true) {
            return this.price + (this.price / 5);
        }
        return this.price;
    }

    public Movie getMovie() {
        return this.movie;
    }
}