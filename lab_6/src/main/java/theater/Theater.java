package theater;

import ticket.Ticket;
import movie.Movie;
import java.util.*;

public class Theater {
    private String name;
    private int theaterAsset;
    private ArrayList<Ticket> ticket;
    private Movie[] movies;

    public Theater(String name, int theaterAsset, ArrayList<Ticket> ticket, Movie[] movies) {
        this.name = name;
        this.theaterAsset = theaterAsset;
        this.ticket = ticket;
        this.movies = movies;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Ticket> getTicket() {
        return this.ticket;
    }

    public Movie[] getMovies() {
        return this.movies;
    }

    public void setTheaterAsset(int profit) {
        this.theaterAsset += profit;
    }

    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop                 : " + this.name);
        System.out.println("Saldo Kas               : " + String.valueOf(this.theaterAsset));
        System.out.println("Jumlah tiket tersedia   : " + ticket.size());
        System.out.print("Daftar Film tersedia    : ");
        for(int i = 0; i < movies.length;i++) {
            if(i == movies.length-1){
                System.out.println(movies[i].getTitle());
            }
            else {
                System.out.print(movies[i].getTitle() + ", ");
            }
        }
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theater) {
        int totalCash = 0;
        for (int i = 0; i < theater.length; i++) {
            totalCash += theater[i].theaterAsset;
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalCash);
        System.out.println("------------------------------------------------------------------");
        for (int i = 0; i < theater.length; i++) {
            System.out.println("Bioskop     : " + theater[i].name);
            System.out.println("Saldo Kas   : " + theater[i].theaterAsset + "\n");
        }
        System.out.println("------------------------------------------------------------------");
    }
}