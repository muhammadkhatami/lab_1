package movie;

public class Movie{
    private String title;
    private String genre;
    private int duration;
    private String rating;
    private String type;

    public Movie(String film, String rating, int duration, String genre, String type){
        this.title = film;
        this.genre = genre;
        this.duration = duration;
        this.rating = rating;
        this.type = type;
    }

    public String getTitle(){
        return this.title;
    }

    public int getRating(){
        if(this.rating.toLowerCase().equals("remaja")){
            return 13;
        }
        else if(this.rating.toLowerCase().equals("dewasa")){
            return 17;
        }
        else{
            return 0;
        }
    }

    public String printMovie(){
        return "------------------------------------------------------------------ \nJudul   : " + this.title
                + "\nGenre   : " + this.genre + "\nDurasi  : " +
                String.valueOf(this.duration) + "\nRating  : " + this.rating + " \nJenis   : Film " + this.type
                + "\n------------------------------------------------------------------";
    }
}