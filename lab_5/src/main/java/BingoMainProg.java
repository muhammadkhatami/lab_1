import java.util.*;

public class BingoMainProg{
    public static void main(String[] args){

        Number[][] numbers = new Number[5][5];
        Number[][] cleanNumbers = new Number[5][5];

        Scanner input = new Scanner(System.in);

        for (int i = 0; i < 5; i++){
            for(int j = 0; j < 5; j++){
                if(j == 5){
                    int value = Integer.parseInt(input.nextLine());
                    numbers[i][j].setValue(value);
                    cleanNumbers[i][j].setValue(value);
                }
                else{
                    int value = Integer.parseInt(input.next());
                    numbers[i][j].setValue(value);
                    cleanNumbers[i][j].setValue(value);
                }
            }
        }

        BingoCard bingo = new BingoCard(numbers, cleanNumbers);
        while(true){
            System.out.print("Masukan perintah	: ");
            String command = input.next();
            if(command.toUpperCase().equals("MARK")){
                int value = Integer.parseInt(input.next());
                System.out.println(bingo.markNum(value));
            }
            else if(command.toUpperCase().equals("INFO")){
                System.out.println(bingo.info());
            }
            else if(command.toUpperCase().equals("RESTART")){
                bingo.restart();
            }
            else if(command.toUpperCase().equals("EXIT")){
                break;
            }
            else{
                System.out.println("perintah salah");
                continue;
            }
        }
        System.exit(0);
    }
}