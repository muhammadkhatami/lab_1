import character.*;
import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();

    public Player find(String name) {
        for (int i = 0; i < player.size(); i++) {
            if (player.get(i).getName().equalsIgnoreCase(name)) {
                return player.get(i);
            }
        }
        return null;
    }

    public String add(String chara, String tipe, int hp) {
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            if (tipe.equalsIgnoreCase("human")) {
                player.add(new Human(chara, hp));
            }
            if (tipe.equalsIgnoreCase("monster")) {
                player.add(new Monster(chara, hp));
            }
            if (tipe.equalsIgnoreCase("magician")) {
                player.add(new Magician(chara, hp));
            }
            return chara + " ditambah ke game";
        }
    }

    public String add(String chara, String tipe, int hp, String roar) {
        String monsterBaru = add(chara, tipe, hp);
        if (tipe.equalsIgnoreCase("Monster") && monsterBaru.equalsIgnoreCase(chara
                + " ditambah ke game")) {
            ((Monster)player.get(0)).setRoar(roar);
        }
        return monsterBaru;
    }

    public String remove(String chara) {
        Player name = find(chara);
        if (name == null) {
            return "Tidak ada " + chara;
        } else {
            for (int i = 0; i < player.size(); i++) {
                if (player.get(i) == name) {
                    player.remove(i);
                }
            }
            return chara + " dihapus dari game";
        }
    }


    public String status(String chara) {
        Player name = find(chara);
        if (name == null) {
            return "Tidak ada " + chara;
        }
        if (diet(chara).equalsIgnoreCase("Belum memakan siapa siapa")) {
            return name.getType() + " " + name.getName()
                    + "\nHP: " + name.getHp()
                    + "\n" + name.condition()
                    + "\n" + diet(chara);
        } else {
            return name.getType() + " " + name.getName()
                    + "\nHP: " + name.getHp()
                    + "\n" + name.condition()
                    + "\n" +  "Memakan " + diet(chara);
        }
    }

    public String status() {
        String status = "";
        for (int i = 0; i < player.size(); i++) {
            status += status(player.get(i).getName());
            if (i != player.size() - 1) {
                status += "\n";
            }
        }
        return status;
    }

    public String diet(String chara) {
        Player name = find(chara);
        if (name == null) {
            return "tidak ada " + chara;
        }
        if (name.dietSize() == 0) {
            return "Belum memakan siapa siapa";
        }
        return name.dietList();
    }

    public String diet() {
        String stats = "";
        for (int i = 0; i < player.size(); i++) {
            stats += diet(player.get(i).getName());
            if (i != player.size()) {
                stats += "\n";
            }
        }
        return stats;
    }

    public String attack(String meName, String enemyName) {
        Player name = find(meName);
        Player mangsa = find(enemyName);
        if (name == null) {
            return "Tidak ada " + meName;
        }
        if (mangsa == null) {
            return "Tidak ada " + enemyName;
        }
        if (mangsa.getCanBeEaten() == true) {
            return mangsa + " Tidak bisa menyerang " + mangsa;
        }
        return name.attack(mangsa);
    }

    public String burn(String meName, String enemyName) {
        Player name = find(meName);
        Player mangsa = find(enemyName);
        if (name == null) {
            return "Tidak ada " + meName;
        }
        if (mangsa == null) {
            return "Tidak ada " + enemyName;
        }
        if (name.getType().equalsIgnoreCase("Magician")) {
            return name.burn(mangsa);
        }
        return  name + " Tidak bisa burn";
    }

    public String eat(String meName, String enemyName) {
        Player name = find(meName);
        Player mangsa = find(enemyName);
        String result = "";
        if (name == null) {
            return "Tidak ada " + meName;
        }
        if (mangsa == null) {
            return "Tidak ada " + enemyName;
        }
        if (name.getCanBeEaten() == true) {
            result = meName + " Tidak bisa memakan " + enemyName;
        }
        if (name instanceof Monster) {
            result = ((Monster)name).eat(mangsa);
        } else if (name instanceof Human) {
            result = ((Human)name).eat(mangsa);
        } else {
            result = ((Magician)name).eat(mangsa);
        }

        if (result.equalsIgnoreCase(name.getName() + " memakan " + mangsa.getName()
                + "\nnyawa " + name.getName() + " kini " + name.getHp())) {
            remove(mangsa.getName());
        }
        return result;
    }

    public String roar(String meName) {
        Player name = find(meName);
        if (name == null) {
            return "Tidak ada " + meName;
        } else {
            if (name instanceof Monster) {
                return ((Monster)name).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
    }
}