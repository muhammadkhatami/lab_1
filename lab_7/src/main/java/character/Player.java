package character;

import java.util.ArrayList;

public class Player {
    protected String name;
    protected int hp;
    protected String type;
    protected ArrayList<Player> diet = new ArrayList<Player>();
    protected boolean matang = false;
    protected boolean canBeEaten = false;

    public Player(String name, int hp, String type) {
        this.name = name;
        if (hp == 0) {
            this.setCanBeEaten(true);
        }
        this.hp = hp;
        this.type = type;
    }

    public boolean getCanBeEaten() {
        return this.canBeEaten;
    }

    public boolean setCanBeEaten(boolean eaten) {
        return this.canBeEaten = eaten;
    }

    public String getName() {
        return this.name;
    }

    public String setTambahanHp(int health) {
        this.hp += health;
        if (hp < 0) {
            hp = 0;
            this.setCanBeEaten(true);
            if (this.getMatang() == true) {
                return "Nyawa " + this.name + " " + this.hp
                        + "\n dan matang";
            } else {
                return "Nyawa " + this.name + " " + this.hp
                        + "\ndan mati";
            }

        }
        return "Nyawa " + this.name + " " + this.hp;
    }

    public int getHp() {
        return this.hp;
    }

    public String getType() {
        return this.type;
    }

    public String setMatang(boolean matang) {
        this.matang = matang;
        return "Nyawa " + this.name + " " + this.hp
                + "\ndan matang";
    }

    public boolean getMatang() {
        return this.matang;
    }

    public String condition() {
        if (this.getMatang() == true) {
            return "Sudah matang";
        } else if (this.getCanBeEaten() == true) {
            return "Sudah meninggal dunia dengan damai";
        } else {
            return "Masih hidup";
        }
    }

    public String dietList() {
        String listKemakan = "";
        for (int i = 0; i < this.diet.size(); i++) {
            if (i == this.diet.size() - 1) {
                listKemakan += diet.get(i).getType() + " " + diet.get(i).getName();
            } else {
                listKemakan += diet.get(i).getType() + " " + diet.get(i).getName() + ", ";
            }
        }
        return listKemakan;
    }

    public int dietSize() {
        return diet.size();
    }

    protected void addDiet(Player mangsa) {
        diet.add(mangsa);
    }

    public boolean canEat(Player mangsa) {
        if (mangsa.getCanBeEaten() == true) {
            return true;
        } else {
            return false;
        }
    }

    public String attack(Player mangsa) {
        if (this.name == null) {
            return "tidak ada " + name;
        }
        if (mangsa == null) {
            return "tidak ada " + mangsa;
        }
        if (this.getCanBeEaten() == true) {
            return mangsa + " tidak bisa menyerang " + mangsa;
        }
        String result = "";
        if (mangsa.getType().equalsIgnoreCase("Magician")) {
            result += mangsa.setTambahanHp(-20);
        } else {
            result += mangsa.setTambahanHp(-10);
        }
        return result;
    }

    public String burn(Player mangsa) {
        return this.burn(mangsa);
    }
}