package character;

public class Monster extends Player {

    private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int hp) {
        super(name, hp * 2, "Monster");
    }

    public String setRoar(String roar) {
        return this.roar = roar;
    }

    public String roar() {
        return this.roar;
    }

    public String eat(Player mangsa) {
        if (mangsa.getHp() == 0) {
            this.setTambahanHp(15);
            this.addDiet(mangsa);
            return this.name + " memakan " + mangsa.getName()
                    + "\nNyawa " + this.name + " kini " + this.hp;
        }
        return this.name + " tidak bisa memakan " + mangsa.getName();
    }

    public String attack(Player mangsa) {
        if (mangsa.getType().equalsIgnoreCase("Magician")) {
            return mangsa.setTambahanHp(-20);
        } else {
            return mangsa.setTambahanHp(-10);
        }
    }
}