package character;

public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, hp, "Magician");
    }

    public String eat(Player mangsa) {
        if (mangsa.getCanBeEaten() == true && mangsa.getMatang() == true
                && mangsa instanceof Monster) {
            this.setTambahanHp(15);
            this.addDiet(mangsa);
            return this.name + " memakan " + mangsa.getName()
                    + "\nNyawa " + this.name + " kini " + this.hp;
        }
        return this.name + " tidak bisa memakan " + mangsa.getName();
    }

    public String burn(Player mangsa) {
        if (mangsa.getType().equalsIgnoreCase("Magician")) {
            if (mangsa.getHp() <= 20) {
                mangsa.setMatang(true);
                return mangsa.setTambahanHp(-20);
            } else {
                return mangsa.setTambahanHp(-20);
            }
        } else {
            if (mangsa.getHp() <= 10) {
                mangsa.setMatang(true);
                return mangsa.setTambahanHp(-10);
            } else {
                return mangsa.setTambahanHp(-10);
            }
        }
    }

    public String attack(Player mangsa) {
        if (mangsa.getType().equalsIgnoreCase("Magician")) {
            return mangsa.setTambahanHp(-20);
        } else {
            return mangsa.setTambahanHp(-10);
        }
    }
}