package lab9.user;

import java.math.BigInteger;
import java.util.ArrayList;
import lab9.event.Event;

/**
* Class representing a user, willing to attend event(s)
*/
public class User {
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getStart().compareTo(newEvent.getFinish()) < 0
                    && events.get(i).getFinish().compareTo(newEvent.getStart()) > 0) {
                return false;
            }
        }
        events.add(newEvent);
        return true;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        // TODO: Implement!
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)                                                                                                          (or... Google. Yeah that works too. OK.)
        ArrayList<Event> newEvents = new ArrayList<Event>(events);
        Event temp;
        boolean done = false;
        while (done == false) {
            for (int i = 1; i < newEvents.size(); i++) {
                done = true;
                if (newEvents.get(i - 1).getStart().compareTo(newEvents.get(i).getStart()) > 0) {
                    temp = newEvents.get(i - 1);
                    newEvents.set(i - 1, newEvents.get(i));
                    newEvents.set(i, temp);
                    done = false;
                }
            }
        }
        return newEvents;
    }

    public BigInteger getTotalCost() {
        BigInteger total = new BigInteger("0");
        for (int i = 0; i < events.size(); i++) {
            total = total.add(events.get(i).getCost());
        }
        return total;
    }
}
