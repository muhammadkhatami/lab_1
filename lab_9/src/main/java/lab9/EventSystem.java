package lab9;

import java.util.ArrayList;
import java.util.Calendar;
import lab9.event.Event;
import lab9.user.User;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    private Event findEvent(String eventName) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getName().equalsIgnoreCase(eventName)) {
                return events.get(i);
            }
        }
        return null;
    }

    private User findUser(String userName) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getName().equalsIgnoreCase(userName)) {
                return users.get(i);
            }
        }
        return null;
    }

    public Event getEvent(String eventName) {
        return findEvent(eventName);
    }

    public User getUser(String userName) {
        return findUser(userName);
    }

    private Calendar createCalender(String[] time) {
        Calendar calendar = Calendar.getInstance();
        int[] date = new int[3];

        for (int i = 0; i < 3; i++) {
            date[i] = Integer.parseInt(time[0].split("-")[i]);
        }

        calendar.set(date[0], date[1], date[2]);
        String[] hour = time[1].split(":");
        for (int j = 0; j < 3; j++) {
            switch (j) {
                case 0 :
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour[0]));
                    break;
                case 1 :
                    calendar.set(Calendar.MINUTE, Integer.parseInt(hour[1]));
                    break;
                case 2 :
                    calendar.set(Calendar.SECOND, Integer.parseInt(hour[2]));
                    break;
                default:
                    break;
            }
        }
        return calendar;
    }
    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        String[] start = startTimeStr.split("_");
        String[] end = endTimeStr.split("_");
        Calendar startTime = createCalender(start);
        Calendar endTime = createCalender(end);

        if (!(startTime.compareTo(endTime) <= 0)) {
            return "waktu yang diinputkan tidak valid!";
        }
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getName().equalsIgnoreCase(name)) {
                return "Event" + name + "sudah ada!";
            }
        }
        events.add(new Event(name, startTime, endTime, costPerHourStr));
        return "Event" + name + "berhasil ditambahkan!";
    }
    
    public String addUser(String name) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getName().equalsIgnoreCase(name)) {
                return "User " + name + " sudah ada!";
            }
        }
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }
    
    public String registerToEvent(String userName, String eventName) {
        User user = findUser(userName);
        Event event = findEvent(eventName);

        if (user == null && event == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else if (user.addEvent(event) == true) {
            return userName + " berencana menghadiri " + eventName + "!";
        }
        return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
    }
}