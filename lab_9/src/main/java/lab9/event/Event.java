package lab9.event;

import java.math.BigInteger;
import java.util.Calendar;

/**
* A class representing an event and its properties
*/
public class Event {
    /** Name of event */
    private String eventName;
    private Calendar start;
    private Calendar finish;
    private BigInteger cost;

    public Event(String eventName, Calendar start, Calendar finish, String cost){
        this.eventName = eventName;
        this.start = start;
        this.finish = finish;
        this.cost = new BigInteger(cost);
    }

    // TODO: Make instance variables for representing beginning and end time of event
    
    // TODO: Make instance variable for cost per hour
    
    // TODO: Create constructor for Event class
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {
        return this.eventName;
    }

    public Calendar getFinish() {
        return finish;
    }

    public Calendar getStart() {
        return start;
    }

    public BigInteger getCost() {
        return cost;
    }

    public String toString() {
        return eventName + "\nWaktu mulai: " + String.format("%02d", start.get(Calendar.DAY_OF_MONTH)) + "-"
                + String.format("%02d", start.get(Calendar.MONTH)) + "-" + start.get(Calendar.YEAR)
                + ", " + String.format("%02d", start.get(Calendar.HOUR_OF_DAY)) + ":"
                + String.format("%02d", start.get(Calendar.MINUTE)) + ":"
                + String.format("%02d", start.get(Calendar.SECOND))
                + "\nWaktu selesai: " + String.format("%02d", finish.get(Calendar.DAY_OF_MONTH)) + "-"
                + String.format("%02d", finish.get(Calendar.MONTH))
                + "-" + String.format("%02d", finish.get(Calendar.YEAR))
                + ", " + String.format("%02d", finish.get(Calendar.HOUR_OF_DAY)) + ":"
                + String.format("%02d", finish.get(Calendar.MINUTE)) + ":"
                + String.format("%02d", finish.get(Calendar.SECOND))
                + "\nBiaya kehadiran: " + this.cost;
    }

    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
