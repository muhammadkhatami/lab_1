package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decryptProcess(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
        String stringResult = "";
        final int messageLength = encryptedMessage.length();
        if (messageLength * 8 > 10000) {
            throw new SizeTooBigException("Your message size is more than 10Kbit");
        }
        if (hugKeyString.length() > 28) {
            throw new KeyTooLongException("Key is too long");
        }
        for (int i = 0; i < messageLength; i++) {
            int eachHugkey = (this.hugKeyString.charAt(i % this.hugKeyString.length()) ^ seed) - 'a';
            int eachMessage = encryptedMessage.charAt(i) ^ eachHugkey;
            stringResult += (char) eachMessage;
        }
        return stringResult;
    }

    public String decrypt(String messageText) {
        return decryptProcess(messageText, HugKey.DEFAULT_SEED);
    }

    public String decrypt(String encryptedMessage, int seed) {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            throw new RangeExceededException("your seed is out of range, it must be in 0 - 36");
        }
        return decryptProcess(encryptedMessage, seed);
    }
}