package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Khatami
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    protected JOptionPane warningPanel;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        messageField = new JTextField();
        keyField = new JTextField();
        seedField = new JTextField();
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        logField = new JTextArea(10, 40);
        logField.setEditable(false);
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
        JFrame frame = new JFrame("CODE SOLVER ENCRYPT AND DECRYPT");
        frame.setSize(1000,200);
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel jPanelLabel = new JPanel(new GridLayout(3,1));
        jPanelLabel.setSize(200, 120);
        jPanelLabel.add(new JLabel("Input\t:"));
        jPanelLabel.add(new JLabel("Key\t:"));
        jPanelLabel.add(new JLabel("Seed\t:"));

        JPanel jPanelInput = new JPanel(new GridLayout(3,1));
        jPanelInput.setSize(200,120);
        jPanelInput.add(messageField);
        jPanelInput.add(keyField);
        jPanelInput.add(seedField);

        JPanel button = new JPanel(new GridLayout(2,1));
        button.setSize(500, 50);
        button.add(encryptButton);
        button.add(decryptButton);

        JScrollPane logScroll = new JScrollPane(logField,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        frame.add(jPanelLabel, BorderLayout.WEST);
        frame.add(jPanelInput, BorderLayout.CENTER);
        frame.add(button,BorderLayout.SOUTH);
        frame.add(logScroll, BorderLayout.EAST);

        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}