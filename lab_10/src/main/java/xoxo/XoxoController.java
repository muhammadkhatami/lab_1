package xoxo;

import jdk.nashorn.internal.scripts.JO;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Khatami
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        this.gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                encyption(gui.getMessageText(), gui.getKeyText(), gui.getSeedText());
            }
        });
        this.gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decryption(gui.getMessageText(), gui.getKeyText(), gui.getSeedText());
            }
        });
    }

    private void decryption(String messageText, String keyText, String seedText) {
        try {
            XoxoDecryption willBeDecrypt = new XoxoDecryption(keyText);
            String decryptedMessage;
            if (seedText.equals("") || seedText.equalsIgnoreCase("DEFAULT_SEED")) {
                decryptedMessage = willBeDecrypt.decrypt(messageText);
            } else {
                decryptedMessage = willBeDecrypt.decrypt(messageText, Integer.parseInt(seedText));
            }
            gui.appendLog(decryptedMessage);
            File file = new File("Decrypted.txt");
            FileWriter writer = new FileWriter(file);
            writer.write(decryptedMessage);
            writer.flush();
            writer.close();
        } catch (NumberFormatException e) {
            gui.warningPanel.showMessageDialog(gui.warningPanel, "Invalid Seed",
                    "Something Wrong!", JOptionPane.ERROR_MESSAGE);
        }catch(RangeExceededException e){
                JOptionPane.showMessageDialog(null, e.getMessage(),
                        "Invalid Seed", JOptionPane.ERROR_MESSAGE);
                gui.appendLog("Failed to decrypt code");
        } catch(SizeTooBigException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "Size Too Big", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to decrypt code");
        } catch(KeyTooLongException e) {
            JOptionPane.showMessageDialog(null, e.getMessage()
                            + ", failed to decrypt.", "Key Exceed Limit"
                    ,JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to decrypt code");
        } catch (Exception e) {
            gui.warningPanel.showMessageDialog(gui.warningPanel, e.getMessage(),
                    "Something Wrong!", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to decrypt code");
        }
    }

    private void encyption(String messageText, String keyText, String seedText) {
        try {
            XoxoEncryption willBeEncrypt = new XoxoEncryption(keyText);
            XoxoMessage encryptedText;
            if (seedText.equals("") || seedText.equalsIgnoreCase("DEFAULT_SEED")) {
                encryptedText = willBeEncrypt.encrypt(messageText);
            } else {
                encryptedText = willBeEncrypt.encrypt(messageText, Integer.parseInt(seedText));
            }
            gui.appendLog(encryptedText.getEncryptedMessage());
            File file = new File("Encrypted.en");
            FileWriter writer = new FileWriter(file);
            writer.write(encryptedText.getEncryptedMessage());
            writer.flush();
            writer.close();
        } catch (NumberFormatException e) {
            gui.warningPanel.showMessageDialog(gui.warningPanel, "Invalid Seed",
                    "Something Wrong!", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to encrypt code");
        }catch(RangeExceededException e){
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "Invalid Seed", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to encrypt code");
        } catch(SizeTooBigException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "Size Too Big", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to encrypt code");
        } catch(KeyTooLongException e) {
            JOptionPane.showMessageDialog(null, e.getMessage()
                            + ", failed to decrypt.", "Key Exceed Limit"
                    ,JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to encrypt code");
        } catch (Exception e) {
            gui.warningPanel.showMessageDialog(gui.warningPanel, e.getMessage(),
                    "Something Wrong!", JOptionPane.ERROR_MESSAGE);
            gui.appendLog("Failed to encrypt code");
        }
    }
    //TODO: Create any methods that you want
}